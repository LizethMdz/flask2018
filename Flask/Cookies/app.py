from flask import Flask, render_template, request, make_response
from werkzeug.security import generate_password_hash, check_password_hash
import os 


app = Flask(__name__)



@app.route("/")
def index():
    return "HOLA MUNDO"

@app.route("/cookies/set")
def set_cookie():
    resp = make_response(render_template("index.html"))
    resp.set_cookie("username", "paco")
    return resp

@app.route("/cookie/read")
def read_cookie():
    username = request.cookies.get("username", None)
    if username == None:
        return "La cookie no existe"
    
    return username


if __name__ == "__main__":
    app.run(debug=True)
