from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    print(__name__)
    return "Hello World"

@app.route("/hola")
def hola():
    return "Hola."

@app.route("/user/<string:user>")
def user(user):
    return "Hola " + user

"""
Variables INT, STRING, 
"""

@app.route("/numero/<int:n>")
def numero(n):
    return "Numero: {}".format(n)

@app.route("/user/<int:id>/<string:username>")
def username(id, username):
    return "Id: {}, Nombre de usuario: {}".format(id, username)

@app.route("/suma/<float:n1>/<float:n2>")
def suma(n1,n2):
    return "El resultado es: {}".format(n1 + n2)

"""
Ruta con valores DEFAULT
Doble decorador
"""
@app.route("/default/")
@app.route("/default/<string:dft>")
def dft(dft = "XD"):
    return "El valor de dft es: " + dft


if __name__ == "__main__":
    """
    Primer parametro que recibe el comando run()
    *Debug - Reinicia el servidor sin estar activando y desactivando en cada cambio
    """
    app.run(debug=True)