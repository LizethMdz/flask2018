from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    print(__name__)
    return "Hello World"

if __name__ == "__main__":
    """
    Primer parametro que recibe el comando run()
    *Debug - Reinicia el servidor sin estar activando y desactivando en cada cambio
    *Puerto - port - 3000
    *Host = "localhost" (0.0.0.0)

    """
    app.run(debug=True)